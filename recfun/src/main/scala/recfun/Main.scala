package recfun
import common._

object Main {
  def main(args: Array[String]) {

    println( countChange(4, 1::2::Nil) )
  }


  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if( c == 0 || c == r ) 1
    else pascal( c-1, r-1 ) + pascal( c, r-1 )
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    def loop( chars: List[Char], heap: List[Char] ): Boolean = {
      if(chars.isEmpty && heap.isEmpty ) true
      else if( chars.isEmpty && !heap.isEmpty ) false
      else if( chars.head == ')' && heap.isEmpty ) false
      else if( chars.head == '(' ) loop( chars.tail, '(' :: heap )
      else if( chars.head == ')' ) loop( chars.tail, heap.tail )
      else loop( chars.tail, heap )

    }

    loop( chars, Nil )
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    if( money == 0 ) 1
    else if( money < 0 || coins.isEmpty ) 0
    else countChange( money - coins.head, coins ) + countChange( money , coins.tail )
  }
}
